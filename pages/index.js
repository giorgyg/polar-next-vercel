import Head from "next/head";
import styles from "../styles/Home.module.scss";
import Image from 'next/image'
import Link from 'next/link'

export const getServerSideProps = async () => {
  const res = await fetch("https://polar-gallery.herokuapp.com/products");
  let products = await res.json();
  return {
    props: {
      products,
    },
  };
};

export default function Home({ products, categories }) {
  function productImage(product) {
    if (product.featured_image) {
      return product.featured_image.formats.medium.url;
    } else {
      return "https://i.stack.imgur.com/y9DpT.jpg";
    }
  }
  return (
    <div className={styles.container}>
      <Head>
        <title>Polar Gallery</title>
        <link rel="icon" href="/assets/logo.png" />
        <meta httpEquiv='cache-control' content='no-cache' />
        <meta httpEquiv='expires' content='0' />
        <meta httpEquiv='pragma' content='no-cache' />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
        <meta name="description" content="Polar gallery - Quality shoes shipping across Indonesia" />
        <meta name="theme-color" content="#000000" />
        <meta name="robots" content="index,follow" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Polar Gallery" />
        <meta property="og:description" content="Polar gallery - Quality shoes shipping across Indonesia" />
        <meta property="og:image" content="%PUBLIC_URL%/assets/logo.png" />
        <meta property="og:url" content="https://polar-next-vercel.vercel.app/" />
        <meta property="og:site_name" content="Polar Gallery" />
      </Head>
      <div className={styles.navbar}>
        <div className={styles.navbar_logo}>
          <Image
            src="/assets/logo.png" // Route of the image file
            height={150} // Desired size with correct aspect ratio
            width={150} // Desired size with correct aspect ratio
            alt="Your Name"
          />
        </div>
      </div>
      <div className={styles.section_title}>Our Products</div>
      <div className={styles.product_container}>
        {products.map((product) => (
          <div key={product.id} className={styles.product}>
            <div>
              <picture>
                <source data-srcset={productImage(product)} type="image/jpg" srcSet={productImage(product)}></source>
                <img className={styles.product_image} data-src={productImage(product)} src={productImage(product)} alt={product}>
                </img>
              </picture>
            </div>
            <div className={styles.product_name}>{product.name}</div>
            <div className={styles.product_price}> IDR {product.price}</div>
          </div>
        ))}
      </div>
      <div className={styles.about_us_container}>
        <div className={styles.section_title}>
          About Us
        </div>
        <div className={styles.about_us_item}>
          Polar Gallery has been serving quality shoes since March 2021, we have provided satisfaction to 2000+ customers in indonesia.
          <br />Established in Cibubur, we inherit local values in our shoes and yet we also embrace advancement in technology.
        </div>
        <div className={styles.about_us_schedule}>
          Deliveries from Monday thru Saturday
          <br />Working Hours 09.00 - 18.00 WIB
        </div>
      </div>
      <div className={styles.separator_container}>
        <div className={styles.separator}></div>
      </div>
      <div className={styles.footer_container}>
        <div className={styles.footer_item}>
          <div className={styles.outside_reference_container}>
            <div className={styles.outside_reference_container_item}>
              <div className={styles.outside_reference_title}>
                Available on
              </div>
              <div className={styles.outside_reference_image}>
                <Link href="https://shopee.co.id/polar_gallery">
                  <a target="_blank">
                    <Image
                        src="/assets/shopee.png" // Route of the image file
                        height={93.75} // Desired size with correct aspect ratio
                        width={66} // Desired size with correct aspect ratio
                        alt="Shopee image"
                      />
                  </a>
                </Link>
              </div>
            </div>
            <div className={styles.outside_reference_container_item}>
              <div className={styles.outside_reference_title}>
                Reach us on
              </div>
              <div className={styles.outside_reference_image}>
                <Link href="https://wa.me/6281283236843">
                  <a target="_blank">
                    <Image
                        src="/assets/whatsapp.png" // Route of the image file
                        height={72.8} // Desired size with correct aspect ratio
                        width={78.8} // Desired size with correct aspect ratio
                        alt="Shopee image"
                      />
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div className={styles.footer_item}>
          Copyright Polar Gallery© 2020
        </div>
      </div>
    </div>
  );
}
